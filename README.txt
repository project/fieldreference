WHAT IT DOES:
-------------
This module adds a new CCK field type for referencing fields in CCK content
types: referenced fields are injected into the content of the referring nodes.

The formatter configuration is straightforward:
  - 'teaser' formatter formats the referenced field as it is configured in its
    own content type for a teaser view.
  - 'full' formatter formats the referenced field as it is configured in its own
    content type for a full view.
  - 'default' formatter formats the referenced field according to the
    referring node: when the referring node is displayed in teaser (respectively
    full) view, the referenced field is formatted as it is configured in its own
    content type for a teaser (respectively full) view.

TO INSTALL:
-----------
Nothing special. Drop the fieldreference folder into the 'sites/all/modules' or
'sites/default/modules' directory of your Drupal installation.

BUGS & ISSUES
-------------
http://drupal.org/project/issues/fieldreference
